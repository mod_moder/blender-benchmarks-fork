License
=======

CC0, free for personal and commercial use

Shader Ball by Jesus Sandoval (Entity)
https://blenderartists.org/u/entitydesigner/activity/portfolio
https://www.artstation.com/entitymotion

HDRI from Poly Haven
https://polyhaven.com/a/brown_photostudio_02


Rendering Images for the Blender Manual
=======================================

Allow execution of scripts when opening the .blend file.

Each object in the Shader Balls collection has a number of materials
assigned. Each of those materials will generate an image for the
manual showing parameter variation. 5 animation frames are rendered,
and composited into a single image.

To render, search for and run the "Render Manual Image" operator.
Images will be written to the Downloads/manual_images folder in the
home directory.
